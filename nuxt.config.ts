// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  modules: ['@pinia/nuxt', "@nuxt/image"],
  ssr: true,
  devtools: { enabled: true },
  css: ['~/assets/scss/main.scss']

})