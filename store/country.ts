import {defineStore} from 'pinia';
const urlApi = 'https://restcountries.com/v3.1';
export const useCountyStore = defineStore('countries', {
    state: () => ({
        countries: [
            {
                name: {
                    official: '',
                },
                flags: {
                    svg: '',
                },
                population: 0,
                cca3: "",
                maps: {
                    googleMaps: ""
                }
            }
        ],
        country: {
            name: {
                official: '',
            },
            flags: {
                svg: '',
            },
            population: 0,
            cca3: "",
            maps: {
                googleMaps: ""
            }
        },
    }),
    actions: {
        async fetchCountries() {
            const {data} = await useFetch(`${urlApi}/all`);
            if (data) {
                this.countries = data.value;
            }
        },
        async fetchCountry(cca3) {
            const {data} = await useFetch(`${urlApi}/alpha/${cca3}`);
            if (data.value) {
                this.country = data.value[0];
            }
        },
        async findCountry(search) {
            const {data} = await useFetch(`${urlApi}/name/${search}`);
            if (data) {
                this.countries = data.value;
            }
        },
    },
})
